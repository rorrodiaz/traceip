FROM openjdk:11-jre-slim
VOLUME /tmp
COPY  target/*.jar /traceip.jar
ENTRYPOINT ["java","-jar","/traceip.jar"]