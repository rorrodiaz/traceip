package com.rodrigodiaz.traceip.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class TraceIPController {

	@Autowired
	TraceIPService traceIPService;

	@PostMapping(value = "/traceip/{ip}", produces = MediaType.APPLICATION_JSON_VALUE)
	public Object traceip(@PathVariable("ip") String ip, HttpServletRequest servletRequest,
			HttpServletResponse response) throws Exception {

		return traceIPService.traceIP(ip);
	}

	@PostMapping(value = "/stats", produces = MediaType.APPLICATION_JSON_VALUE)
	public Object stats(HttpServletRequest servletRequest, HttpServletResponse response) throws Exception {

		return traceIPService.stats();
	}
}
