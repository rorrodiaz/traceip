package com.rodrigodiaz.traceip.controller;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.logging.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.data.redis.connection.jedis.JedisConnectionFactory;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.rodrigodiaz.traceip.dto.StatDTO;
import com.rodrigodiaz.traceip.dto.TraceIPDTO;
import com.rodrigodiaz.traceip.model.CountryInfo;
import com.rodrigodiaz.traceip.model.Ip2Country;
import com.rodrigodiaz.traceip.repository.CountryInfoRepository;
import com.rodrigodiaz.traceip.repository.Ip2CountryRepository;
import com.rodrigodiaz.traceip.util.DistanceUtil;

@Service
public class TraceIPService {

	private static Logger log = Logger.getLogger(TraceIPService.class.getName());

	@Autowired
	CountryInfoRepository countryInfoRepository;

	@Autowired
	Ip2CountryRepository ip2CountryRepository;
	
	@Value("${apiIP2Country}")
	String apiIP2Country;
	
	@Value("${apiRestcountries}")
	String apiRestcountries;
	
	
	@Bean
	JedisConnectionFactory jedisConnectionFactory() {

		JedisConnectionFactory jedisConFactory = new JedisConnectionFactory();
		jedisConFactory.setHostName("redis");
		jedisConFactory.setPort(6379);
		return jedisConFactory;
		// return new JedisConnectionFactory();
	}

	@Bean
	public RedisTemplate<String, Object> redisTemplate() {
		RedisTemplate<String, Object> template = new RedisTemplate<>();
		template.setConnectionFactory(jedisConnectionFactory());
		return template;
	}

	public TraceIPDTO traceIP(String ip) throws Exception {
		RestTemplate restTemplate = new RestTemplate();
		TraceIPDTO traceIPDTO = new TraceIPDTO();
		Ip2Country ip2Country = ip2CountryRepository.findById(ip).orElse(null);
		if (ip2Country == null) {
			log.info("call to" + apiIP2Country + ip);
			ip2Country = restTemplate.getForObject(apiIP2Country + ip, Ip2Country.class);
			ip2Country.setIp(ip);
			ip2CountryRepository.save(ip2Country);
		}

		CountryInfo country = countryInfoRepository.findById(ip2Country.getCountryCode()).orElse(null);

		if (country == null) {
			log.info("call to " + apiRestcountries + ip2Country.getCountryCode());
			country = restTemplate.getForObject(apiRestcountries + ip2Country.getCountryCode(),
					CountryInfo.class);
			country.setDistanceToBsAs(DistanceUtil.distanceToBsAs(country.getLat(), country.getLng()) / 1000);
		}
		country.incCall();
		countryInfoRepository.save(country);

		traceIPDTO.setIp(ip);
		traceIPDTO.setName(ip2Country.getCountryName());
		traceIPDTO.setIso(ip2Country.getCountryCode());
		traceIPDTO.setLanguages(country.getLanguages());
		String times = "";
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy HH:mm:ss");
		LocalDateTime dateTime = LocalDateTime.now();
		traceIPDTO.setDate(dateTime.format(formatter));

		formatter = DateTimeFormatter.ofPattern("HH:mm:ss (z)");

		for (String zone : country.getTimezones()) {
			ZonedDateTime z = ZonedDateTime.now(ZoneId.of(zone));
			times = times + z.format(formatter) + ",";
		}
		traceIPDTO.setTimes(times.split(","));
		StringBuilder dis = new StringBuilder();
		dis.append(country.getDistanceToBsAs().intValue()).append("kms ").append("(").append(DistanceUtil.LAT_BSAS)
				.append(",").append(DistanceUtil.LNG_BSAS).append(")").append(" a ").append("(")
				.append(country.getLat()).append(",").append(country.getLng()).append(")");
		traceIPDTO.setDistanceToBsAs(dis.toString());

		traceIPDTO.setCurrencies(country.getCurrencies());

		return traceIPDTO;

	}

	public StatDTO stats() throws Exception {
		Double min = Double.MAX_VALUE;
		Double max = Double.MIN_VALUE;
		Double avg = 0d;
		Double calls = 0d;
		StatDTO stat = new StatDTO();
		for (CountryInfo c : countryInfoRepository.findAll()) {
			if (c.getDistanceToBsAs() > max) {
				max = c.getDistanceToBsAs();
			}
			if (c.getDistanceToBsAs() < min) {
				min = c.getDistanceToBsAs();
			}
			avg = avg + c.getDistanceToBsAs() * c.getCalls();
			calls = calls + c.getCalls();
		}
		avg = avg / calls;
		stat.setMaxDistance(max);
		stat.setMinDistance(min);
		stat.setAvgDistance(avg);

		log.info("MAX " + max);
		log.info("MIN " + min);
		log.info("AVG " + avg);
		return stat;
	}
}
