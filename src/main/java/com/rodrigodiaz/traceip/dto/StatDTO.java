package com.rodrigodiaz.traceip.dto;

public class StatDTO {

	
	public StatDTO() {

	}

	private Double minDistance;
	private Double maxDistance;
	private Double avgDistance;

	public Double getMinDistance() {
		return minDistance;
	}

	public void setMinDistance(Double minDistance) {
		this.minDistance = minDistance;
	}

	public Double getMaxDistance() {
		return maxDistance;
	}

	public void setMaxDistance(Double maxDistance) {
		this.maxDistance = maxDistance;
	}

	public Double getAvgDistance() {
		return avgDistance;
	}

	public void setAvgDistance(Double avgDistance) {
		this.avgDistance = avgDistance;
	}
}
