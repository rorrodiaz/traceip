package com.rodrigodiaz.traceip.dto;

import com.rodrigodiaz.traceip.model.Currency;
import com.rodrigodiaz.traceip.model.Language;

public class TraceIPDTO {

	public TraceIPDTO() {

	}

	private String ip;
	private String date;
	private String name;
	private String iso;
	private Language[] languages;

	private String[] times;
	private String distanceToBsAs;
	private Currency[] currencies;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getIso() {
		return iso;
	}

	public void setIso(String iso) {
		this.iso = iso;
	}

	public Language[] getLanguages() {
		return languages;
	}

	public void setLanguages(Language[] languages) {
		this.languages = languages;
	}

	public String[] getTimes() {
		return times;
	}

	public void setTimes(String[] times) {
		this.times = times;
	}

	public String getDistanceToBsAs() {
		return distanceToBsAs;
	}

	public void setDistanceToBsAs(String distanceToBsAs) {
		this.distanceToBsAs = distanceToBsAs;
	}


	public String getIp() {
		return ip;
	}

	public void setIp(String ip) {
		this.ip = ip;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public Currency[] getCurrencies() {
		return currencies;
	}

	public void setCurrencies(Currency[] currencies) {
		this.currencies = currencies;
	}
}
