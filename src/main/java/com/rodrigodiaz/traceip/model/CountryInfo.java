package com.rodrigodiaz.traceip.model;

import java.io.Serializable;

import org.springframework.data.redis.core.RedisHash;

import com.fasterxml.jackson.annotation.JsonAlias;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@RedisHash("Countries")
@JsonIgnoreProperties(ignoreUnknown = true)
public class CountryInfo implements Serializable {

	private static final long serialVersionUID = 3436758891737387990L;

	public CountryInfo() {
		this.calls = 0L;
	}

	@JsonAlias("alpha2Code")
	private String id;

	private Double distanceToBsAs;
	private Long[] latlng;

	private Language[] languages;
	private Currency[] currencies;

	private String[] timezones;
	private Long calls;

	public Long getCalls() {
		return calls;
	}

	public void setCalls(Long calls) {
		this.calls = calls;
	}

	public Double getDistanceToBsAs() {
		return distanceToBsAs;
	}

	public void setDistanceToBsAs(Double distanceToBsAs) {
		this.distanceToBsAs = distanceToBsAs;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public Long[] getLatlng() {
		return latlng;
	}

	public void setLatlng(Long[] latlng) {
		this.latlng = latlng;
	}

	public Long getLat() {
		return this.latlng[0];
	}

	public Long getLng() {
		return this.latlng[1];
	}

	public void incCall() {
		this.calls++;
	}

	public Language[] getLanguages() {
		return languages;
	}

	public void setLanguages(Language[] languages) {
		this.languages = languages;
	}

	public String[] getTimezones() {
		return timezones;
	}

	public void setTimezones(String[] timezones) {
		this.timezones = timezones;
	}

	public Currency[] getCurrencies() {
		return currencies;
	}

	public void setCurrencies(Currency[] currencies) {
		this.currencies = currencies;
	}

}