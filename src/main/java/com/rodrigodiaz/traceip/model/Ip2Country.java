package com.rodrigodiaz.traceip.model;

import java.io.Serializable;

import org.springframework.data.annotation.Id;
import org.springframework.data.redis.core.RedisHash;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@RedisHash("Countries")
@JsonIgnoreProperties(ignoreUnknown = true)
public class Ip2Country implements Serializable  {

	private static final long serialVersionUID = 3433385087748195064L;
	
	@Id
	private String ip;
	private String countryCode;
	private String countryName;

	public Ip2Country() {
	}

	public String getIp() {
		return ip;
	}

	public void setIp(String ip) {
		this.ip = ip;
	}

	public String getCountryCode() {
		return countryCode;
	}

	public void setCountryCode(String countryCode) {
		this.countryCode = countryCode;
	}

	public String getCountryName() {
		return countryName;
	}

	public void setCountryName(String countryName) {
		this.countryName = countryName;
	}
}
