package com.rodrigodiaz.traceip.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.rodrigodiaz.traceip.model.CountryInfo;

@Repository
public interface CountryInfoRepository extends CrudRepository<CountryInfo, String> {}