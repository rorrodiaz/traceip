package com.rodrigodiaz.traceip.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.rodrigodiaz.traceip.model.Ip2Country;

@Repository
public interface Ip2CountryRepository extends CrudRepository<Ip2Country, String> {
	
	Ip2Country findByIp(String ip);
}